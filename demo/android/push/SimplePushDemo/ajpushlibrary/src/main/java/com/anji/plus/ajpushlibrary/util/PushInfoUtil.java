package com.anji.plus.ajpushlibrary.util;

import android.content.Intent;
import android.os.Bundle;

import com.anji.plus.ajpushlibrary.AppSpPushLog;
import com.anji.plus.ajpushlibrary.AppSpPushConstant;
import com.anji.plus.ajpushlibrary.model.NotificationMessageModel;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 * 点击通知栏后获取服务器传来的信息
 * </p>
 */
public class PushInfoUtil {

    //华为推送接收数据
    public static NotificationMessageModel getHWIntentData(Intent intent) {
        NotificationMessageModel hwNotificationMessage = new NotificationMessageModel();
        if (null != intent) {
            // 获取的值做打点统计
//            String msgid = intent.getStringExtra("_push_msgid");
//            String cmdType = intent.getStringExtra("_push_cmd_type");
//            int notifyId = intent.getIntExtra("_push_notifyid", -1);

            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                HashMap<String, String> map = new HashMap<>();
                for (String key : bundle.keySet()) {
                    String content = String.valueOf(bundle.get(key));
                    map.put(key, content);
                    AppSpPushLog.i("hw receive data from push, key = " + key + ", content = " + content);
                }
                JSONObject json = new JSONObject(map);
                hwNotificationMessage.setNotificationExtras(json.toString());
            }
        }
        return hwNotificationMessage;
    }

    //小米
    public static NotificationMessageModel getXMIntentData(Intent intent) {
        NotificationMessageModel xmNotificationMessage = new NotificationMessageModel();
        if (null != intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                xmNotificationMessage = (NotificationMessageModel) bundle.getSerializable(AppSpPushConstant.HNOTIFICATIONMESSAGE);
            }
        }
        return xmNotificationMessage;
    }

    //vivo
    public static NotificationMessageModel getVivoIntentData(Intent intent) {
        NotificationMessageModel vivoNotificationMessage = new NotificationMessageModel();
        //获取自定义透传参数值
        if (null != intent) {
            HashMap<String, String> map = new HashMap<>();
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                for (String key : bundle.keySet()) {
                    String content = String.valueOf(bundle.get(key));
                    map.put(key, content);
                    AppSpPushLog.i("vivo receive data from push, key = " + key + ", content = " + content);
                }
                JSONObject json = new JSONObject(map);
                vivoNotificationMessage.setNotificationExtras(json.toString());
            }
        }
        return vivoNotificationMessage;
    }

    //oppo
    public static NotificationMessageModel getOppoIntentData(Intent intent) {
        NotificationMessageModel oppoNotificationMessage = new NotificationMessageModel();
        //获取自定义透传参数值
        if (null != intent) {
            HashMap<String, String> map = new HashMap<>();
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                for (String key : bundle.keySet()) {
                    String content = String.valueOf(bundle.get(key));
                    map.put(key, content);
                    AppSpPushLog.i("oppo receive data from push, key = " + key + ", content = " + content);
                }
                JSONObject json = new JSONObject(map);
                oppoNotificationMessage.setNotificationExtras(json.toString());
            }
        }
        return oppoNotificationMessage;
    }


    //极光
    public static NotificationMessageModel getJPushIntentData(Intent intent) {
        NotificationMessageModel notificationMessageModel = new NotificationMessageModel();
        if (null != intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                notificationMessageModel = (NotificationMessageModel) bundle.getSerializable(AppSpPushConstant.HNOTIFICATIONMESSAGE);
            }
        }
        return notificationMessageModel;
    }
}
