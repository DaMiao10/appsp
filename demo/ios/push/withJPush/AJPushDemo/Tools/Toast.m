//
//  Toast.m
//  AJPushDemo
//
//  Created by kean_qi on 2021/3/18.
//

#import "Toast.h"
#import <QuartzCore/CALayer.h>
//设备的高
#define size_height1 [UIScreen mainScreen].bounds.size.height
//设备的宽
#define size_width1 [UIScreen mainScreen].bounds.size.width
static Toast * _toast = nil;
static NSInteger shake = 0;
static NSTimer *timer = nil;
@interface Toast ()

@property (retain, nonatomic) UILabel *label;

@end

@implementation Toast
- (id)initWithText:(NSString*)text
{
    self = [super init];
    if (self) {
        _text = [text copy];
        // Initialization code
        UIFont *font = [UIFont systemFontOfSize:16];
        
        CGRect textRect = [_text boundingRectWithSize:(CGSizeMake(280, MAXFLOAT)) options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName: font} context:nil];
        CGSize textSize = textRect.size;
        
        //leak;
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, textSize.width, textSize.height)];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
        label.font = font;
        label.text = _text;
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines = 0;
        label.shadowColor = [UIColor darkGrayColor];
        label.shadowOffset = CGSizeMake(1, 1);
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
        CGRect rect;
        rect.size = CGSizeMake(textSize.width + 20, textSize.height + 10);
        rect.origin = CGPointMake((size_width1-rect.size.width)/2, 380);
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
        [self setFrame:rect];
        [self addSubview:label];
    }
    return self;
}


- (id)initWithUpText:(NSString*)text
{
    self = [super init];
    if (self) {
        _text = [text copy];
        // Initialization code
        UIFont *font = [UIFont systemFontOfSize:16];
        
        CGRect textRect = [_text boundingRectWithSize:(CGSizeMake(280, MAXFLOAT)) options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName: font} context:nil];
        CGSize textSize = textRect.size;
        
        //leak;
        _label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, textSize.width, textSize.height)];
        _label.backgroundColor = [UIColor clearColor];
        _label.textColor = [UIColor whiteColor];
        _label.font = font;
        _label.text = _text;
        _label.numberOfLines = 0;
        _label.textAlignment = NSTextAlignmentCenter;
        _label.shadowColor = [UIColor darkGrayColor];
        _label.shadowOffset = CGSizeMake(1, 1);
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
        CGRect rect;
        rect.size = CGSizeMake(textSize.width + 20, textSize.height + 10);
        rect.origin = CGPointMake((size_width1-rect.size.width)/2, 104);
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
        [self setFrame:rect];
        [self addSubview:_label];
    }
    return self;
}

+(Toast *)makeText:(NSString *)text{
    @synchronized(self){
        if(_toast == nil){
            _toast = [[Toast alloc]initWithText:text];
        }
    }
    return _toast;
}

+(Toast *)makeUpText:(NSString *)text{
    @synchronized(self){
        if(_toast == nil){
            _toast = [[Toast alloc] initWithUpText:text];
        }
    }
    return _toast;
}

-(void)showWithType:(enum AJTimeType)type{
    if (type == AJLongTime) {
        _time = 3.0f;
    }else{
        _time = 1.0f;
    }
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    
    timer = [NSTimer timerWithTimeInterval:(_time/4.0f)  target:self selector:@selector(removeToast) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    [window addSubview:self];
}

-(void)removeToast
{
    [UIView animateWithDuration:_time animations:^{
        if (_toast.alpha!=0.0f)
            _toast.alpha -= 0.3f;
    } completion:^(BOOL finished) {
        [_toast setAlpha:0];
        [_toast removeFromSuperview];
        _toast = nil;
    }];
}

+ (void)classRemoveToast{
    [UIView animateWithDuration:2.0f animations:^{
        if (_toast.alpha!=0.0f)
            _toast.alpha -= 0.3f;
    } completion:^(BOOL finished) {
        [_toast setAlpha:0];
        [_toast removeFromSuperview];
        _toast = nil;
    }];
}

+(void)aj_makeUpText:(NSString *)text showWithType:(enum AJTimeType)type;{
    // init
    @synchronized (self) {
        if(_toast == nil){
            shake=0;
            _toast = [[Toast alloc] initWithUpText:text];
        }else shake=1;
    }
    // show
    CGFloat time;
    if (type == AJLongTime) {
        time = 3.0f;
    }else{
        time = 1.0f;
    }
    if (shake) {
        dispatch_async(dispatch_get_main_queue(), ^{
            _toast.label.text = text;
        });
        //创建动画
        CAKeyframeAnimation * keyAnimaion = [CAKeyframeAnimation animation];
        keyAnimaion.keyPath = @"transform.rotation";
        keyAnimaion.values = @[@(-10 / 180.0 * M_PI),@(10 /180.0 * M_PI),@(-10/ 180.0 * M_PI),@(0/ 180.0 * M_PI)];//度数转弧度
        
        keyAnimaion.removedOnCompletion = YES;
        keyAnimaion.fillMode = kCAFillModeForwards;
        keyAnimaion.duration = 0.15;
        keyAnimaion.repeatCount = 1;
        [_toast.layer addAnimation:keyAnimaion forKey:nil];
    }else{
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        
        timer = [NSTimer timerWithTimeInterval:(time/4.0f)  target:self selector:@selector(classRemoveToast) userInfo:nil repeats:NO];
        [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
        [window addSubview:_toast];
    }
}

@end
