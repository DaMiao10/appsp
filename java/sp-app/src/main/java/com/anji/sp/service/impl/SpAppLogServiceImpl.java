package com.anji.sp.service.impl;

import com.alibaba.fastjson.JSON;
import com.anji.sp.mapper.SpAppDeviceMapper;
import com.anji.sp.mapper.SpAppLogMapper;
import com.anji.sp.model.po.SpAppDevicePO;
import com.anji.sp.model.po.SpAppLogPO;
import com.anji.sp.model.vo.SpAppLogVO;
import com.anji.sp.service.SpAppDeviceService;
import com.anji.sp.service.SpAppLogService;
import com.anji.sp.util.DateUtils;
import com.anji.sp.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class SpAppLogServiceImpl implements SpAppLogService {
    @Resource
    private SpAppLogMapper spAppLogMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void logTableArchive() {
        Map<String, String> threeMonthsAgoTimestamp = null;
        try {
            threeMonthsAgoTimestamp = DateUtils.getMonthsAgoTimestamp(-3);
        } catch (ParseException e) {
            log.error("ParseException", e);
        }
        //创建表
        log.info("创建表--------------->{},{}","sp_app_log", JSON.toJSONString(threeMonthsAgoTimestamp));
        spAppLogMapper.createTable(threeMonthsAgoTimestamp);
        //复制数据至归档表
        log.info("复制数据至归档表----------------------------");

        spAppLogMapper.copyArchiveData(threeMonthsAgoTimestamp);
        //删除原表数据
        log.info("删除sp_app_log表三个月前数据-------------");
        spAppLogMapper.deleteArchiveData(threeMonthsAgoTimestamp);
    }
}
