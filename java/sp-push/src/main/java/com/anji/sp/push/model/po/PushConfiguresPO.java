package com.anji.sp.push.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 推送配置项
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("push_configures")
public class PushConfiguresPO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 应用ID
     */
    private Long appId;

    /**
     * 应用唯一key
     */
    private String appKey;

    /**
     * 服务加密使用
     */
    private String secretKey;

    /**
     * 极光appkey
     */
    private String jgAppKey;

    /**
     * 极光masterSecret
     */
    private String jgMasterSecret;

    /**
     * 华为appId
     */
    private String hwAppId;

    /**
     * 华为AppSecret
     */
    private String hwAppSecret;

    /**
     * 华为推送标题（默认建议应用名）
     */
    private String hwDefaultTitle;

    /**
     * oppo appId
     */
    private String opAppId;

    /**
     * oppo AppSecret
     */
    private String opAppSecret;

    /**
     * oppo 推送标题（默认建议应用名）
     */
    private String opDefaultTitle;

    /**
     * oppo AppKey
     */
    private String opAppKey;

    /**
     * oppo Master Secret
     */
    private String opMasterSecret;

    /**
     * vivo appId
     */
    private String voAppId;

    /**
     * vivo AppSecret
     */
    private String voAppSecret;

    /**
     * vivo 推送标题（默认建议应用名）
     */
    private String voDefaultTitle;

    /**
     * vivo AppKey
     */
    private String voAppKey;

    /**
     * 小米appId
     */
    private String xmAppId;

    /**
     * 小米AppSecret
     */
    private String xmAppSecret;

    /**
     * 小米推送标题（默认建议应用名）
     */
    private String xmDefaultTitle;

    /**
     * 华为AppKey
     */
    private String xmAppKey;

    /**
     * 包名
     */
    private String packageName;

    /**
     * 0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG
     */
    private Integer enableFlag;

    /**
     * 0--未删除 1--已删除 DIC_NAME=DEL_FLAG
     */
    private Integer deleteFlag;


    /**
     * 创建者
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    /**
     * 更新者
     */
    private Long updateBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateDate;

    /**
     * 备注信息
     */
    private String remarks;

    /**  是否开启厂商通道 默认 0 */
    private Integer channelEnable;



}
