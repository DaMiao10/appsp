package com.anji.plus.ajpushlibrary.base;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.anji.plus.ajpushlibrary.AppSpPushConstant;
import com.anji.plus.ajpushlibrary.http.AppSpPushPostData;
import com.anji.plus.ajpushlibrary.util.PhoneUtil;
import com.anji.plus.ajpushlibrary.util.SPUtil;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 * Appsp
 * </p>
 */
public class AppSpPushBaseRequest {
    protected Context context;
    protected String appKey;

    public AppSpPushBaseRequest(Context context, String appKey) {
        this.context = context;
        this.appKey = appKey;
    }

    protected AppSpPushPostData getPostEncryptData() {
        AppSpPushPostData appSpPushPostData = new AppSpPushPostData();
        String regId = (String) SPUtil.get(context, AppSpPushConstant.jPushRegId, "");
//        String regId = AppSpPushConstant.jPushRegId;
        Log.d("dengmin"," getPostEncryptData regId "+regId);
        try {
            String deviceId = PhoneUtil.getDeviceId(context);

            Log.i("dengmin", deviceId);
            appSpPushPostData.put("appKey", appKey);
            appSpPushPostData.put("manuToken", AppSpPushConstant.pushToken);
            appSpPushPostData.put("deviceType", AppSpPushConstant.brandType);
            appSpPushPostData.put("registrationId", regId);
            appSpPushPostData.put("deviceId", deviceId);
            appSpPushPostData.put("alias", "");
            appSpPushPostData.put("brand", Build.MODEL);
            appSpPushPostData.put("osVersion", Build.VERSION.RELEASE);
        } catch (Exception e) {

        }

        return appSpPushPostData;
    }
}
