import request from './axios'

// 推送配置查询
export function getPushConfig(appId) {
  const data = {
      appId: appId
    }
  return request({
      url: '/pushConfigures/select',
      method: 'post',
      data
  })
}

// 保存推送配置
export function savePushConfig(data) {
  return request({
    url: '/pushConfigures/save',
    method: 'post',
    data
  })
}

// 获取推送列表
export function getPushList(data) {
  return request({
    url: '/pushMessage/queryByPage',
    method: 'post',
    data
  })
}

// 查询推送历史详情
export function getPushDetail(data) {
  return request({
    url: '/pushHistory/queryByMsgId',
    method: 'post',
    data
  })
}

// 推送消息 - 全推
export function doPushAllMsg(data) {
  return request({
    url: '/pushWeb/pushAll',
    method: 'post',
    data
  })
}

// 推送消息 - 精准，根据registrationId
export function doPushBatchMsg(data) {
  return request({
    url: '/pushWeb/pushBatch',
    method: 'post',
    data
  })
}